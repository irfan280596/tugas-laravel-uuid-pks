<?php

namespace App\Http\Controllers;

use App\Post;
use App\Events\PostCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar post berhasil ditampilkan',
            'data' => $posts

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll = $request->all();

        $validator = Validator::make($requestAll, [
            'title' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()){
            return response() ->json($validator->errors(), 400);
        }
        $posts = Post::create([
            'title' => $request ->title,
            'description' => $request ->description,

        ]);

        event(new PostCreated($posts));

        if ($posts){

        return response()->json([
            'success' => true,
            'message' => 'Data  post berhasil dibuat',
            'data' => $posts
        ], 200);
    
    }
    return response()->json([
        'success' => false,
        'message' => 'Data post gagal dibuat',
        
    ],409);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
