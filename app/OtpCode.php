<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\userr;

class OtpCode extends Model
{
     //
     protected $guarded = [];
     protected $primaryKey = 'id' ;
 
     protected $keyType = 'string' ;
 
     public $incrementing = false;
 
     protected static function boot() {
         parent::boot();
 
         static::creating( function($model){
             if( empty($model->{$model->getKeyName()})){
                 $model->{$model->getKeyName()}= Str::uuid();
             }
         });
     }
     public function userr(){
         return $this->belongsTo(App\userr);
     }
 }
 