<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/userr', function (Request $request){
    return $request->userr();
});

Route::apiResource('/posts','PostController');

Route::apiResource('/role','RoleController');

Route::apiResource('/comment','CommentsController');

Route::post('/auth/register','Auth\RegisterController')->name('auth.register');

