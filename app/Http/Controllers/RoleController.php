<?php

namespace App\Http\Controllers;

use App\roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = roles::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar post berhasil ditampilkan',
            'data' => $posts

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAll = $request->all();

        $validator = Validator::make($requestAll, [
            'title' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()){
            return response() ->json($validator->errors(), 400);
        }
        // $post = Post::create([
        //     'title' => $request ->title,
        //     'description' => $request ->description,

        // ]);

        $post = Post::create ($requestAll);

        return response()->json([
            'success' => true,
            'message' => 'Data daftar post berhasil ditambahkan',
            'data' => $posts
        ], 200);
    
    return response()->json([
        'success' => false,
        'message' => 'Data post gagal dibuat',
        
    ],409);
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
