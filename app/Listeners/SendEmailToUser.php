<?php

namespace App\Listeners;

use App\Events\OtpCodeStoredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpCodeStoredEvent  $event
     * @return void
     */
    public function handle(OtpCodeStoredEvent $event)
    {
        //
    }
}
