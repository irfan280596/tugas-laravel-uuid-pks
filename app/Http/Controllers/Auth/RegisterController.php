<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'email' => 'required|unique|email',
            'username' => 'required|unique'
        ]);

        if ($validator->fails()){
            return response() ->json($validator->errors(), 400);
        }
    }
}
